import express from 'express';
import { z } from 'zod';
import { todosCollection } from '../db.js'
import { ObjectId } from 'mongodb';


const router = express.Router();

const TodoSchema = z.object({
    task: z.string(),
    checked: z.boolean(),
  })

let todos = [
    { task: 'Wash Dishes', checked: true },
    { task: 'Clean House', checked: true }
  ];

//GET /todos
router.get('/', async (req, res) => {
    const todos = await todosCollection.find().toArray()
    res.status(200).send(todos);
});

// GET /todos/:id
router.get('/:id', async (req, res) => {
        const todoID = req.params.id;

        if (!ObjectId.isValid(todoID)) return res.status(400).send('Invalid ID')
    
        const foundTodoItem = await todosCollection.findOne({ _id: new ObjectId (todoID) })
        if (foundTodoItem == null) return res.status(404).send('Not Found')
    
      res.status(200).send(foundTodoItem)
    });

// POST /todos 
router.post('/', async (req, res) => {
    const newTodoItem = req.body;

    const parsedResult = TodoSchema.safeParse(newTodoItem)
  
    if (!parsedResult.success) {
      return res.status(400).send(parsedResult.error)
    }
    const result = await todosCollection.insertOne(parsedResult.data);
    const todoItem = await todosCollection.findOne({ 
        _id: new ObjectId(result.insertedId)
    })
    res.status(201).send(todoItem);
})

// PATCH /todos/:id 
router.patch('/:id', async(req, res) => {
const TodoId = req.params.id;
  const checked = req.body.checked

  if (!ObjectId.isValid(TodoId)) return res.status(400).send('ID is not valid')

  const foundTodoItem = await todosCollection.findOne({ 
    _id: new ObjectId (TodoId)
})

console.log(foundTodoItem)

  if (foundTodoItem == null) return res.status(404).send('Not Found')


const result = await todosCollection.updateOne({ 
    _id: new ObjectId (TodoId) 
}, {
     $set: { checked } 
    })
    

    const updatedTodoItem= await todosCollection.findOne({ 
        _id: new ObjectId (TodoId)
    }) 
    
  res.status(200).send(updatedTodoItem)

})

// DELETE /todos/:id
router.delete('/:id', async (req, res) => {

    const TodoId = req.params.id;

  if (!ObjectId.isValid(TodoId)) return res.status(400).send('ID is not valid')

  const foundTodoItem = await todosCollection.findOne({ 
    _id: new ObjectId (TodoId)
})

console.log(foundTodoItem)

  if (foundTodoItem == null) return res.status(404).send('Not Found')


const result = await todosCollection.deleteOne({ 
    _id: new ObjectId (TodoId) 
})
    const DeletedTodoItem= await todosCollection.findOne({ 
        _id: new ObjectId (TodoId)
    }) 
    
  res.status(200).send(DeletedTodoItem)

})

export default router;